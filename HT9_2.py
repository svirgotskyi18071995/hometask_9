# Створіть класс Triangle (трикутник), який задається (ініціалізується) трьома точками (обʼєкти классу Point)
# .# Реалізуйте перевірку даних, аналогічно до класу Line. Визначет метод, що містить площу трикутника.
class Point:
    def x_getter(self):
        return self._x

    def x_setter(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError
        else:
            self._x = value

    x = property(x_getter, x_setter)

    def y_getter(self):
        return self._y

    def y_setter(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError
        else:
            self._y = value

    y = property(y_getter, y_setter)

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f'Point [{self.x}:{self.y}]'


class Triangle:

    def point_1_getter(self):
        return self._point_1

    def point_1_setter(self, value):
        if not isinstance(value, Point):
            raise TypeError
        else:
            self._point_1 = value

    point_1 = property(point_1_getter, point_1_setter)

    def point_2_getter(self):
        return self._point_2

    def point_2_setter(self, value):
        if not isinstance(value, Point):
            raise TypeError
        else:
            self._point_2 = value

    point_2 = property(point_2_getter, point_2_setter)

    def point_3_getter(self):
        return self._point_3

    def point_3_setter(self, value):
        if not isinstance(value, Point):
            raise TypeError
        else:
            self._point_3 = value

    point_3 = property(point_3_getter, point_3_setter)

    def __init__(self, point_1, point_2, point_3):
        if isinstance(point_1, Point) and isinstance(point_2, Point) and isinstance(point_3, Point):
            self.point_1 = point_1
            self.point_2 = point_2
            self.point_3 = point_3
        else:
            raise TypeError

    def __str__(self):
        return f'Triangle [{self.point_1} - {self.point_2} - {self.point_3}]'

    def area_calculation(self):
        side1_length = ((self.point_2.x - self.point_1.x) ** 2 + (self.point_2.y - self.point_1.y) ** 2) ** 0.5
        side2_length = ((self.point_3.x - self.point_2.x) ** 2 + (self.point_3.y - self.point_2.y) ** 2) ** 0.5
        side3_length = ((self.point_1.x - self.point_3.x) ** 2 + (self.point_1.y - self.point_3.y) ** 2) ** 0.5

        half_perimeter = (side3_length + side2_length + side1_length) / 2

        res = (half_perimeter * (half_perimeter - side1_length) * (half_perimeter - side2_length) * (
                half_perimeter - side3_length)) ** 0.5
        return res


p1 = Point(1, 2)
p2 = Point(3, 7)
p3 = Point(6, 2)

triangle_1 = Triangle(p1, p2, p3)
print(f'Your area is ----> {Triangle.area_calculation(triangle_1)}')
