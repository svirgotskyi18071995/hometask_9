# Доопрацюйте класс Point так, щоб в атрибути x та y обʼєктів цього класу можна
# було записати тільки обʼєкти класу int або float

print('-----------------------------------------------------------------------------------')
print('----------------------------------------- Task 1 ----------------------------------')
print('-----------------------------------------------------------------------------------')


class Point:
    def x_getter(self):
        print(self._x)
        return self._x

    def x_setter(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError
        else:
            self._x = value

    x = property(x_getter, x_setter)

    def y_getter(self):
        print(self._y)
        return self._y

    def y_setter(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError
        else:
            self._y = value

    y = property(y_getter, y_setter)

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f'Point [{self.x}:{self.y}]'


class Line:
    begin = None
    end = None

    def __init__(self, point1, point2):
        if isinstance(point1, Point) and isinstance(point2, Point):
            self.begin = point1
            self.end = point2
        else:
            raise TypeError

    def __str__(self):
        return f'Line [{self.begin} - {self.end}]'


p1 = Point(1, 2)
p2 = Point(3, 7)
line1 = Line(p1, p2)
print(Line.__str__(line1))

